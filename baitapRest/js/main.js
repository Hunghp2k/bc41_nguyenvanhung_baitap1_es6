let tinhTB = (...listDiem) => {
    let sum = 0;
    listDiem.forEach((diem) => {
        sum += diem * 1;
    })
    return sum / listDiem.length;
}
let tinhKhoi1 = () => {
    const toan = document.getElementById('inpToan').value;
    const ly = document.getElementById('inpLy').value;
    const hoa = document.getElementById('inpHoa').value;
    let tbKhoi1 = tinhTB(toan, ly, hoa)
    document.getElementById('tbKhoi1').innerText = tbKhoi1.toFixed(2);
}
let tinhKhoi2 = () => {
    const van = document.getElementById('inpVan').value;
    const su = document.getElementById('inpSu').value;
    const dia = document.getElementById('inpDia').value;
    const english = document.getElementById('inpEnglish').value;
    let tbKhoi2 = tinhTB(van, su, dia, english)
    document.getElementById('tbKhoi2').innerText = tbKhoi2.toFixed(2);
}
