const jumpText = () => {
    let headingText = document.querySelector('.heading').innerText;
    let chars = [...headingText];
    let contentHTML = '';
    chars.forEach((item) => {
        let content = `<span>${item}</span>`;
        contentHTML += content;
    })
    document.querySelector('.heading').innerHTML = contentHTML;
}

jumpText();